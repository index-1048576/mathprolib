# mathprolib
[![star](https://gitee.com/index-1048576/mathprolib/badge/star.svg?theme=white)](https://gitee.com/index-1048576/mathlib/stargazers)  [![fork](https://gitee.com/index-1048576/mathprolib/badge/fork.svg?theme=white)](https://gitee.com/index-1048576/mathlib/members)

11种编程语言通用的数学运算专用库，使用简单，内含三角学、分数、科学计算法等实用功能。


# 下载
| 编程语言 | 下载方法/命令                          |
|--------|----------------------------------------|
| Python | pip install mathprolib                 |
| Go     | go get -u github.com/wrmdcxy/mathprolib|
| C      | unknown                                |
| C++    | unknown                                |
| Java   | 在Java分支下载                          |
| JS     | unknown                                |
| Ruby   | unknown                                |
| Lua    | unknown                                |
| Julia  | unknown                                |
| Lisp   | unknown                                |
| VB.NET | unknown


# 分数

### 注意事项
1. 使用分数时，分母不能是0
2. 使用带分数时，分子必须小于分母，而且第一个参数(integer)必须是int类型
3. 父类Fraction没有运算功能，不要使用Fraction类，要使用VulgarFraction和MixedFraction类

解释：
1. 分数也是一个除法算式，有一条规则：不能除以0。则分数同理。
2. 小学五年级下有一条概念：由<b><a href="https://baike.baidu.com/item/%E6%95%B4%E6%95%B0/1293937">整数</a></b>和<b><a href="https://baike.baidu.com/item/真分数/2666229">真分数</a>合成的数叫带分数。</b>
3. 父类Fraction只有些许功能，没有运算功能。用户可以自行浏览代码。

<br><br>
<h5>以下内容以Python为例</h5>
<br>

## 分数操作
### 分数运算和比较

下面的代码展现了mathprolib的[分数的基本运算](https://baike.baidu.com/item/%E5%88%86%E6%95%B0/2067623#8)

```python
from mathprolib.Fraction import VulgarFraction,MixedFraction
f1=VulgarFraction(1,3)#三分之一
f2=MixedFraction(1,2,3)#一又三分之二
print(f1+f2)
print(f1-f2)
print(f1*f2)
print(f1/f2)
print(f1>f2)
print(f1<f2)
print(f1==f2)
print(f1>=VulgarFraction(1,3))

# output:
# 2 
#
# -4
# —
# 3
#
# 5
# —
# 9
#
# 1
# —
# 5
# 
# False
# True
# False
# True
```

### 约分、通分
下面展示了如何在mathprolib中[约分](https://baike.baidu.com/item/%E7%BA%A6%E5%88%86)、[通分](https://baike.baidu.com/item/%E9%80%9A%E5%88%86)
```python
from mathprolib.Fraction import VulgarFraction,MixedFraction
vgf1=VulgarFraction(5,20)# 5/20
vgf1.reduction()#约分至最简
vgf1 # 1/4
vgf2=VulgarFraction(4,20) #4/20
vgf2.reduction(2)#用2约分
vgf2 # 2/10
vgf2.reduction(5)#用5约分，无法约分，抛出警告: WARNING:Reduction failed.Cannot reduction by 5
vgf2 # 2/10
mxf1=MixedFraction(1,3,6)# 1 3/6
mxf1.reduction()#约分至最简
mxf1 # 1 1/2


vgf3=VulgarFraction(1,4)
mxf2=MixedFraction(1,2,3)
mxf2=vgf3.common(mxf2)
vgf3# 3/12
mxf2# 20/12
```

### 普通分数和带分数之间的互化
下面是[普通分数和带分数之间的互化](https://baike.baidu.com/item/%E5%81%87%E5%88%86%E6%95%B0/2666302?fr=aladdin#3)的操作
```python
from mathprolib.Fraction import VulgarFraction

# 假分数化成带分数
fra1=VulgarFraction(8,5)#8/5
mxfra1=fra1.toMixed()
mxfra1 # 1 3/5
```
```python
from mathprolib.Fraction import MixedFraction

# 带分数化成假分数
mxf1=MixedFraction(1,9,11)
fra=mxf1.toVulgar()
fra # 20/11
```

### 连分数
下面展示了如何在mathprolib中使用连分数
```python
from mathprolib.Fraction import VulgarFraction,MixedFraction
vgf1 = VulgarFraction(VulgarFraction(5,2),3) # 5/2/3 = 5/6
vgf2 = vgf1
print(vgf1+vgf2) # 5/3
print(vgf2-vgf1) # 0
print(vgf1*vgf2) # 25/36
# 带分数的连分数还在开发中
```
##### __温馨小提示:不要使用mathprolib的Fraction来制作无穷级数，因为转float过程中需要递归qwq__

# 科学计数法
下面是[科学计数法的用法示例](https://baike.baidu.com/item/%E7%A7%91%E5%AD%A6%E8%AE%B0%E6%95%B0%E6%B3%95/1612882)，这里以Python为例子
```python
from mathprolib import Scientific
s = Scientific.Scientific_notation(3,5)
print(s)
print((a*a)**2) #相当于a**4的意思
print(a+666)
```
 
# 多项式